import {Component} from 'react'
import './Products.scss'

class Products extends Component {
	render() {

		return (
			<div className='coffee-machines'>
				{this.props.children}
			</div>
		)
	}
}




/* 
const CoffeeMachines = ({children})=>{
    return(
        <div>
            {children}
        </div>
    )
} */

export default Products